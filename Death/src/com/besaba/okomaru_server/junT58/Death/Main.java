package com.besaba.okomaru_server.junT58.Death;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onPD(PlayerDeathEvent e){
		List<ItemStack> stacklist = e.getDrops();
		List<ItemStack> stackbuf = new ArrayList<ItemStack>();
		for(ItemStack stack : stacklist){
			if(stack.getType().equals(Material.DIAMOND)){
				stackbuf.add(stack);
			}
			if(stack.getType().equals(Material.DIAMOND_BLOCK)){
				stackbuf.add(stack);
			}
		}
		for(ItemStack stack : stackbuf){
			stacklist.remove(stack);
		}
	}

}
